package de.foodsharing.ui.baskets

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.MutableLiveData
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.BasketAPI
import de.foodsharing.model.Basket
import de.foodsharing.services.BasketService
import de.foodsharing.services.ProfileService
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.test.createRandomProfile
import de.foodsharing.test.createRandomUser
import de.foodsharing.utils.CurrentUserLocation
import de.foodsharing.utils.UserLocation
import io.reactivex.Observable
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.ArgumentMatchers.anyDouble
import org.mockito.ArgumentMatchers.anyInt
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class NearbyBasketsViewModelTest {
    @Mock
    lateinit var basketService: BasketService

    @Mock
    lateinit var profileService: ProfileService

    @Mock
    lateinit var currentUserLocation: CurrentUserLocation

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch baskets`() {
        val randomProfile = createRandomProfile(hasAddress = true)
        whenever(profileService.current()) doReturn Observable.just(randomProfile)
        whenever(currentUserLocation.currentUserCoordinates) doReturn (MutableLiveData())
        val userLocation = UserLocation(profileService, currentUserLocation)

        val creator = createRandomUser()
        val baskets = mutableListOf<Basket>()
        val numBaskets = 10
        for (i in 1..numBaskets) {
            baskets.add(createRandomBasket(creator))
        }

        whenever(basketService.listClose(anyDouble(), anyDouble(), anyInt())) doReturn
            Observable.just(BasketAPI.BasketListResponse().apply {
                this.baskets = baskets
            })

        val viewModel = NearbyBasketsViewModel(basketService, userLocation)
        val nearbyBaskets = viewModel.baskets.value!!

        Assert.assertEquals(numBaskets, nearbyBaskets.size)

        for (basket in nearbyBaskets) {
            val actualDistance = randomProfile.coordinates?.distanceTo(basket.basket.toCoordinate())
            Assert.assertTrue(Math.abs(actualDistance!! - basket.distance!!) < 0.01)
        }
    }

    @Test
    fun `fetch baskets without address`() {
        val randomProfile = createRandomProfile(hasAddress = false)
        whenever(profileService.current()) doReturn Observable.just(randomProfile)
        whenever(currentUserLocation.currentUserCoordinates) doReturn (MutableLiveData())
        val userLocation = UserLocation(profileService, currentUserLocation)

        val creator = createRandomUser()
        val baskets = mutableListOf<Basket>()
        val numBaskets = 10
        for (i in 0..numBaskets) {
            baskets.add(createRandomBasket(creator))
        }

        whenever(basketService.listClose(anyDouble(), anyDouble(), anyInt())) doReturn
            Observable.just(BasketAPI.BasketListResponse().apply {
                this.baskets = baskets
            })

        val viewModel = NearbyBasketsViewModel(basketService, userLocation)
        val nearbyBaskets = viewModel.baskets.value!!

        // If the coordinate is not set in the profile, we currently expect that no baskets are returned
        Assert.assertEquals(0, nearbyBaskets.size)
    }
}
