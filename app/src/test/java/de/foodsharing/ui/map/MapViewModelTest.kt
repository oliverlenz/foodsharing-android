package de.foodsharing.ui.map

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.whenever
import de.foodsharing.api.MapAPI
import de.foodsharing.model.Basket
import de.foodsharing.model.CommunityMarker
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.test.configureTestSchedulers
import de.foodsharing.test.createRandomBasket
import de.foodsharing.test.createRandomCommunityMarker
import de.foodsharing.test.createRandomFoodSharePoint
import io.reactivex.Observable.just
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.MockitoAnnotations

class MapViewModelTest {

    @Mock
    lateinit var mapAPI: MapAPI

    @get:Rule
    var rule: TestRule = InstantTaskExecutorRule()

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        configureTestSchedulers()
    }

    @Test
    fun `fetch markers`() {
        val foodSharePoints = mutableListOf<FoodSharePoint>()
        for (i in 1..10) {
            foodSharePoints.add(createRandomFoodSharePoint())
        }
        val baskets = mutableListOf<Basket>()
        for (i in 1..10) {
            baskets.add(createRandomBasket())
        }
        val communities = mutableListOf<CommunityMarker>()
        for (i in 1..10) {
            communities.add(createRandomCommunityMarker())
        }

        val response = MapAPI.MapResponse(1, foodSharePoints, baskets, communities)

        whenever(mapAPI.coordinates()) doReturn just(response)

        val viewModel = MapViewModel(mapAPI)
        val markers = viewModel.markers.value!!
        Assert.assertEquals(markers.first, foodSharePoints)
        Assert.assertEquals(markers.second, baskets)
    }
}
