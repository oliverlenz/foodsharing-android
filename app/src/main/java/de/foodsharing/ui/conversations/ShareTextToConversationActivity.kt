package de.foodsharing.ui.conversations

import android.os.Bundle
import android.view.MenuItem
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import de.foodsharing.R
import kotlinx.android.synthetic.main.activity_share_text_to_conversation.toolbar
import javax.inject.Inject

class ShareTextToConversationActivity : DaggerAppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_share_text_to_conversation)

        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setTitle(R.string.send_message_to)
        }

        supportFragmentManager.beginTransaction().add(R.id.fragment_container, ConversationsFragment()).commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
