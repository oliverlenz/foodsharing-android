package de.foodsharing.ui.pickups

import android.content.Context
import com.stfalcon.chatkit.utils.DateFormatter
import de.foodsharing.R
import java.util.Calendar
import java.util.Date

class FuturePickupDateFormatter(val context: Context) {

    fun format(date: Date): String {
        val calendar = Calendar.getInstance()
        calendar.time = date
        val time = if (calendar[Calendar.MINUTE] == 0) {
            DateFormatter.format(date, "HH a")
        } else {
            DateFormatter.format(date, DateFormatter.Template.TIME)
        }

        return when {
            DateFormatter.isToday(date) -> "${context.getString(R.string.date_today)} $time"
            DateFormatter.isCurrentYear(date) ->
                "${DateFormatter.format(date, "EEEE, d MMMM")}, $time"
            else -> "${DateFormatter.format(date, DateFormatter.Template.STRING_DAY_MONTH_YEAR)} $time"
        }
    }
}
