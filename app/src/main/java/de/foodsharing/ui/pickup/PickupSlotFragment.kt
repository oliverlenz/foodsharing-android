package de.foodsharing.ui.pickup

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.StorePickupSlot
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.conversation.ConversationActivity
import kotlinx.android.synthetic.main.fragment_pickup_slot.view.*

class PickupSlotFragment : BaseFragment(), Injectable {

    companion object {
        const val EXTRA_SLOT = "slot"
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_pickup_slot, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        (arguments?.getSerializable(EXTRA_SLOT) as? StorePickupSlot)?.let {
            setSlot(view, it)
        }
    }

    private fun setSlot(view: View, pickupSlot: StorePickupSlot) {
        view.user_name.text = pickupSlot.profile.name
        view.button_user_chat.setOnClickListener {
            ConversationActivity.start(requireContext(), pickupSlot.profile.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }

        if (!pickupSlot.profile.landline.isNullOrBlank() || !pickupSlot.profile.mobile.isNullOrBlank()) {
            view.button_user_call.visibility = View.VISIBLE

            if (!pickupSlot.profile.landline.isNullOrBlank() && !pickupSlot.profile.mobile.isNullOrBlank()) {
                // two available numbers: show a popup menu
                val contextThemeWrapper = ContextThemeWrapper(context, R.style.PopupMenuOverlapAnchor)
                val popup = PopupMenu(contextThemeWrapper, view.button_user_call, Gravity.TOP or Gravity.END)
                popup.menuInflater.inflate(R.menu.pickup_slot_call_menu, popup.menu)

                popup.setOnMenuItemClickListener { item ->
                    when (item.itemId) {
                        R.id.pickup_slot_call_mobile_item -> startPhoneCall(pickupSlot.profile.mobile)
                        R.id.pickup_slot_call_landline_item -> startPhoneCall(pickupSlot.profile.landline)
                    }
                    false
                }

                view.button_user_call.setOnClickListener {
                    context?.let {
                        popup.show()
                    }
                }
            } else {
                // only one number: start the dial intent directly
                val number =
                    if (pickupSlot.profile.landline.isNullOrBlank()) pickupSlot.profile.landline
                    else pickupSlot.profile.mobile
                number?.let { n ->
                    view.button_user_call.setOnClickListener {
                        startPhoneCall(n)
                    }
                }
            }
        } else {
            view.button_user_call.visibility = View.INVISIBLE
        }
    }

    /**
     * Starts the dial intent for a phone call.
     */
    private fun startPhoneCall(number: String) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:$number"))
        startActivity(intent)
    }
}
