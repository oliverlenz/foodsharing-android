package de.foodsharing.ui.pickup

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Store
import de.foodsharing.model.StorePickup
import de.foodsharing.services.AuthService
import de.foodsharing.services.PickupService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.safeLet
import io.reactivex.Single
import java.io.IOException
import java.util.Date
import javax.inject.Inject

class PickupViewModel @Inject constructor(
    authService: AuthService,
    private val pickupsService: PickupService
) : BaseViewModel() {
    fun pickup(): LiveData<StorePickup?> = pickup
    fun isLoading(): LiveData<Boolean> = isLoading
    fun error(): LiveData<Event<Int>> = error

    // input data: a pickup is uniquely identified by the store id and the date
    var store: Store? = null
    var date: Date? = null
        set(value) {
            if (field != value) {
                field = value
                fetch()
            }
        }

    // status and output data
    private val pickup = MutableLiveData<StorePickup?>()
    private val isLoading = MutableLiveData<Boolean>().apply {
        value = true
    }
    private val error = MutableLiveData<Event<Int>>()

    private var currentUserId: Int? = null

    init {
        request(authService.currentUser(), {
            currentUserId = it.id
        }, {
            handleError(it)
        })
    }

    fun fetch() {
        safeLet(store?.id, date) { storeId, date ->
            isLoading.value = true

            request(pickupWithoutCurrentUser(pickupsService.getPickup(storeId, date)), {
                isLoading.value = false
                pickup.value = it
            }, {
                isLoading.value = false
                handleError(it)
            })
        } ?: run {
            pickup.value = null
        }
    }

    private fun handleError(throwable: Throwable) {
        isLoading.value = false
        throwable.printStackTrace()
        when (throwable) {
            is IOException -> {
                // Network Error
                error.postValue(Event(R.string.error_no_connection))
            }
            else -> {
                // HttpException or unknown exception
                error.postValue(Event(R.string.error_unknown))
            }
        }
    }

    /**
     * Maps the observed pickup onto one that does not include the current user in the occupied slots.
     */
    private fun pickupWithoutCurrentUser(single: Single<StorePickup>): Single<StorePickup> =
        single.map { pickup ->
            StorePickup(
                date = pickup.date, isAvailable = pickup.isAvailable,
                totalSlots = pickup.totalSlots,
                occupiedSlots = pickup.occupiedSlots.filter {
                    it.profile.id != currentUserId
                }
            )
        }
}
