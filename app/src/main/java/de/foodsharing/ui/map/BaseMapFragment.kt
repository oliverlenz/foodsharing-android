package de.foodsharing.ui.map

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Point
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import de.foodsharing.R
import de.foodsharing.model.Coordinate
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.utils.DEFAULT_MAP_ZOOM
import de.foodsharing.utils.LocationFinder
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.OsmdroidUtils.toCoordinate
import de.foodsharing.utils.OsmdroidUtils.toGeoPoint
import de.foodsharing.utils.Utils
import kotlinx.android.synthetic.main.fragment_base_map.map_location_button
import kotlinx.android.synthetic.main.fragment_base_map.view.map
import kotlinx.android.synthetic.main.fragment_base_map.view.map_location_button
import org.osmdroid.util.GeoPoint
import org.osmdroid.views.MapView
import org.osmdroid.views.overlay.mylocation.SimpleLocationOverlay

open class BaseMapFragment : BaseFragment() {

    companion object {
        private const val STATE_COORDINATE = "coordinate"
        private const val STATE_ZOOM = "zoom"
    }

    protected lateinit var mapView: MapView
    protected lateinit var locationOverlay: SimpleLocationOverlay

    private var locationCallbackCancelable: (() -> Unit)? = null
    private val permissionRequestLauncher =
        registerForActivityResult(ActivityResultContracts.RequestPermission()) { granted ->
            if (granted) {
                context?.applicationContext?.let {
                    LocationFinder.instance.initialise(it)
                    findLocation()
                }
            }
        }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(getLayoutId(), container, false)

        // set tile map provider
        mapView = view.map

        OsmdroidUtils.setupMapView(mapView, preferences.allowHighResolutionMap)

        context?.let {
            val bitmap = Utils.getBitmapFromResourceId(it, R.drawable.location_circle) ?: return@let
            locationOverlay = SimpleLocationOverlay(bitmap)
            locationOverlay.setLocation(null)
            locationOverlay.setPersonIcon(bitmap, Point(bitmap.width / 2, bitmap.height / 2))
            mapView.overlays.add(locationOverlay)
        }

        view.map_location_button.setOnClickListener {
            onFindLocationClick()
        }

        if (savedInstanceState != null) {
            savedInstanceState.getParcelable<Coordinate>(STATE_COORDINATE)?.let {
                val zoom = savedInstanceState.getDouble(STATE_ZOOM)
                updateLocation(it, zoom, animate = false)
            }
        }

        mapView.setOnTouchListener { _, _ ->
            stopLocationUpdates()
            false
        }

        return view
    }

    protected fun canRestoreMapCamera(savedInstanceState: Bundle?): Boolean {
        return savedInstanceState != null &&
            savedInstanceState.containsKey(STATE_ZOOM) &&
            savedInstanceState.containsKey(STATE_COORDINATE)
    }

    protected open fun getLayoutId(): Int {
        return R.layout.fragment_base_map
    }

    override fun onSaveInstanceState(outState: Bundle) {
        if (::mapView.isInitialized) {
            outState.putParcelable(STATE_COORDINATE, mapView.mapCenter.toCoordinate())
            outState.putDouble(STATE_ZOOM, mapView.zoomLevelDouble)
        }
        super.onSaveInstanceState(outState)
    }

    fun getMapCenter(): Coordinate {
        return mapView.mapCenter.toCoordinate()
    }

    private fun findLocation() {
        if (LocationFinder.instance.isLocationAvailable()) {
            startLocationUpdates()
        } else {
            showLocationNotification()
        }
    }

    private fun startLocationUpdates() {
        locationCallbackCancelable?.let { it() }

        val callback = LocationFinder.instance.requestLocation {
            val point = GeoPoint(it)
            updateLocation(point)
            locationOverlay.setLocation(point)
        }
        map_location_button.supportImageTintList =
            ColorStateList.valueOf(ContextCompat.getColor(requireContext(), R.color.colorSecondary))
        locationCallbackCancelable = {
            map_location_button.supportImageTintList = null
            callback()
        }
    }

    private fun stopLocationUpdates() {
        locationCallbackCancelable?.let { it() }
    }

    private fun onFindLocationClick() {
        val permission = Manifest.permission.ACCESS_FINE_LOCATION
        if (context?.let { ContextCompat.checkSelfPermission(it, permission) } == PackageManager.PERMISSION_GRANTED) {
            findLocation()
        } else {
            // request real-time permissions (api >= 23)
            permissionRequestLauncher.launch(permission)
        }
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        stopLocationUpdates()
        mapView.onDetach()
    }

    /**
     * Sets the location and zoom of the map.
     */
    fun updateLocation(point: Coordinate, zoom: Double? = null, animate: Boolean? = null) {
        updateLocation(point.toGeoPoint(), zoom, animate)
    }

    private fun updateLocation(point: GeoPoint, zoom: Double? = null, animate: Boolean? = null) {
        val requestedZoom = zoom ?: DEFAULT_MAP_ZOOM
        val locationZoom = if (mapView.zoomLevelDouble > requestedZoom) {
            mapView.zoomLevelDouble
        } else {
            requestedZoom
        }
        val locationAnimate = animate ?: true
        if (locationAnimate) {
            mapView.controller.animateTo(point, locationZoom, null)
        } else {
            mapView.controller.setZoom(locationZoom)
            mapView.controller.setCenter(point)
        }
    }

    /**
     * Notifies the user that location services are disabled.
     */
    private fun showLocationNotification() {
        AlertDialog.Builder(context ?: return)
                .setMessage(R.string.map_location_services_not_enabled)
                .setPositiveButton(R.string.map_open_location_settings) {
                        _, _ -> context?.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
    }
}
