package de.foodsharing.ui.baskets

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_basket.view.item_description
import kotlinx.android.synthetic.main.item_basket.view.item_distance
import kotlinx.android.synthetic.main.item_basket.view.item_picture
import kotlinx.android.synthetic.main.item_basket.view.item_title_text

class MyBasketListAdapter(
    private val onClickListener: (Basket) -> Unit,
    val context: Context
) : RecyclerView.Adapter<MyBasketListAdapter.BasketHolder>() {
    private var baskets: List<Basket> = emptyList()

    fun setBaskets(newBaskets: List<Basket>) {
        val diffResult = DiffUtil.calculateDiff(object : DiffUtil.Callback() {
            override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition].id == newBaskets[newItemPosition].id
            }

            override fun getOldListSize(): Int {
                return baskets.size
            }

            override fun getNewListSize(): Int {
                return newBaskets.size
            }

            override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
                return baskets[oldItemPosition] == newBaskets[newItemPosition]
            }
        })

        baskets = newBaskets
        diffResult.dispatchUpdatesTo(this)
    }

    override fun getItemCount(): Int = baskets.size

    override fun onBindViewHolder(holder: BasketHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(baskets[position])
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): BasketHolder {
        return BasketHolder(
                parent.inflate(
                        R.layout.item_basket,
                        false
                ), onClickListener, context
        )
    }

    class BasketHolder(
        val view: View,
        private val onClickListener: (Basket) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var basket: Basket? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            basket?.let {
                onClickListener(it)
            }
        }

        fun bind(basket: Basket) {
            this.basket = basket

            // view.item_creation_date.text = formatter.format(basket.createdAt)
            view.item_description.text = basket.description

            if (!basket.picture.isNullOrEmpty()) {
                view.item_picture.visibility = View.VISIBLE
                val pictureUrl = "$BASE_URL/images/basket/${basket.picture}"
                Glide.with(context)
                        .load(pictureUrl)
                        .apply(RequestOptions().transform(CenterCrop(), RoundedCorners(8)))
                        .error(R.drawable.basket_default_picture)
                        .into(view.item_picture)
            } else {
                view.item_picture.visibility = View.GONE
            }

            view.setBackgroundColor(
                ContextCompat.getColor(context, R.color.white)
            )
            view.item_title_text.text = context.getString(R.string.basket_title_mine)
            view.item_distance.visibility = View.GONE
        }
    }
}
