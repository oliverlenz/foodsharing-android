package de.foodsharing.ui.baskets

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.basket.BasketActivity
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import kotlinx.android.synthetic.main.fragment_my_baskets.no_published_baskets_label
import kotlinx.android.synthetic.main.fragment_my_baskets.progress_bar
import kotlinx.android.synthetic.main.fragment_my_baskets.recycler_view
import kotlinx.android.synthetic.main.fragment_my_baskets.view.recycler_view
import javax.inject.Inject

class MyBasketsFragment : BaseFragment() {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: MyBasketsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: MyBasketListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_my_baskets, container, false)
        view.recycler_view.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = MyBasketListAdapter({ basket ->
            BasketActivity.start(requireContext(), basket.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        view.recycler_view.adapter = adapter

        bindViewModel()

        return view
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                no_published_baskets_label.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
            } else {
                progress_bar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showMessage(getString(it))
        })

        viewModel.baskets.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                recycler_view.visibility = View.GONE
                no_published_baskets_label.visibility = View.VISIBLE
            } else {
                recycler_view.visibility = View.VISIBLE
                no_published_baskets_label.visibility = View.GONE

                adapter.setBaskets(it)
            }
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
