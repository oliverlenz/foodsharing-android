package de.foodsharing.ui.main

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import de.foodsharing.R
import de.foodsharing.ui.conversations.ConversationsFragment

class MainPagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    private val conversationsFragment = ConversationsFragment()
    private val giveFragment = GiveFragment()
    private val takeFragment = TakeFragment()

    override fun getCount(): Int = 3

    override fun getItem(position: Int): Fragment = when (position) {
        0 -> conversationsFragment
        1 -> takeFragment
        2 -> giveFragment
        else -> throw Exception("invalid tab id!")
    }

    fun getPageTextView(position: Int): Int = when (position) {
        0 -> R.layout.tab_conversation
        1 -> R.layout.tab_take
        2 -> R.layout.tab_give
        else -> throw Exception("invalid tab id!")
    }
}
