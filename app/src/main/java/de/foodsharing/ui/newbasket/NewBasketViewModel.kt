package de.foodsharing.ui.newbasket

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.model.Basket
import de.foodsharing.model.Coordinate
import de.foodsharing.model.Profile
import de.foodsharing.services.BasketService
import de.foodsharing.services.PreferenceManager
import de.foodsharing.services.ProfileService
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import java.io.File
import java.io.IOException
import javax.inject.Inject

class NewBasketViewModel @Inject constructor(
    private val basketService: BasketService,
    private val preferenceManager: PreferenceManager,
    profileService: ProfileService
) : BaseViewModel() {

    val isLoading = MutableLiveData<Boolean>().apply { value = true }
    val showError = MutableLiveData<Event<Int>>()
    val profile = MutableLiveData<Profile>()
    val basketPublished = MutableLiveData<Event<Int>>()

    init {
        request(profileService.current(), {
            isLoading.value = false
            profile.value = it
        }, {
            isLoading.value = false
            handleError(it)
        })
    }

    fun publish(
        description: String,
        phone: String?,
        mobile: String?,
        contactByMessage: Boolean,
        lifetime: Int,
        location: Coordinate?,
        picture: File?
    ) {
        val contactTypes = ArrayList<Int>()
        val contactByPhone = phone != null && mobile != null

        if (contactByPhone) contactTypes.add(Basket.CONTACT_TYPE_PHONE)
        if (contactByMessage) contactTypes.add(Basket.CONTACT_TYPE_MESSAGE)

        preferenceManager.wasLastContactByMessage = contactByMessage
        preferenceManager.wasLastContactByPhone = contactByPhone

        isLoading.value = true
        request(basketService.create(
            description,
            contactTypes.toTypedArray(),
            phone,
            mobile,
            lifetime,
            location?.lat,
            location?.lon
        ), {
            val basket = it.basket!!

            if (picture != null) {
                request(basketService.setPicture(basket.id, picture), {
                    basketPublished.value = Event(basket.id)
                }, { e ->
                    handleError(e)
                })
            } else basketPublished.value = Event(basket.id)

            isLoading.value = false
        }, {
            handleError(it)
            isLoading.value = false
        })
    }

    private fun handleError(error: Throwable) {
        val stringRes = if (error is IOException) {
            R.string.error_no_connection
        } else {
            error.printStackTrace()
            R.string.error_unknown
        }
        showError.value = Event(stringRes)
    }
}
