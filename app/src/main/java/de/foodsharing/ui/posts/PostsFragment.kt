package de.foodsharing.ui.posts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import de.foodsharing.R
import de.foodsharing.api.PostsAPI
import de.foodsharing.di.Injectable
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import kotlinx.android.synthetic.main.fragment_posts.create_post_widgets
import kotlinx.android.synthetic.main.fragment_posts.post_edit_text
import kotlinx.android.synthetic.main.fragment_posts.posts_container
import kotlinx.android.synthetic.main.fragment_posts.posts_label
import kotlinx.android.synthetic.main.fragment_posts.send_message_button
import javax.inject.Inject

class PostsFragment : BaseFragment(), Injectable {

    private var label = ""
    private lateinit var postListAdapter: PostListAdapter

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: PostsViewModel by viewModels { viewModelFactory }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_posts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        context?.let { context ->
            postListAdapter = PostListAdapter(context, viewModel.currentUserId)
            posts_container.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = postListAdapter
            }
            bindViewModel()
        }
    }

    private fun bindViewModel() {
        viewModel.posts().observe(viewLifecycleOwner) { postList ->
            postListAdapter.setPosts(postList)
            posts_label.visibility =
                if (label == "" || (postList.isEmpty() && viewModel.mayPost().value == false)) GONE else VISIBLE
        }

        viewModel.mayPost().observe(viewLifecycleOwner) { mayPost ->
            create_post_widgets.visibility = if (mayPost) VISIBLE else GONE
            posts_label.visibility =
                if (label == "" || (!mayPost && viewModel.posts().value.isNullOrEmpty())) GONE else VISIBLE
        }

        viewModel.mayDelete().observe(viewLifecycleOwner) {
            postListAdapter.mayDelete = it
        }

        viewModel.error().observe(viewLifecycleOwner, EventObserver {
            showMessage(getString(it))
        })
    }

    fun setTarget(target: PostsAPI.Target, targetId: Int, label: String? = null) {
        postListAdapter.deletePost = { postId ->
            viewModel.deletePost(target, targetId, postId)
        }

        send_message_button.setOnClickListener {
            viewModel.sendPost(target, targetId, post_edit_text.text.toString()) {
                // on success
                post_edit_text.setText("")
            }
        }

        label?.let {
            this.label = it
            posts_label.text = it
        }

        viewModel.fetchPosts(target, targetId)
    }
}
