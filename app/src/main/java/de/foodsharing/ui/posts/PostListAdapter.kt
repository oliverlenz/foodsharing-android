package de.foodsharing.ui.posts

import android.content.Context
import android.text.format.DateUtils.FORMAT_SHOW_DATE
import android.text.format.DateUtils.FORMAT_SHOW_TIME
import android.text.format.DateUtils.FORMAT_SHOW_WEEKDAY
import android.text.format.DateUtils.formatDateTime
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import de.foodsharing.R
import de.foodsharing.model.Post
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.Utils
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_post.view.post_body
import kotlinx.android.synthetic.main.item_post.view.post_delete_button
import kotlinx.android.synthetic.main.item_post.view.post_pictures
import kotlinx.android.synthetic.main.item_post.view.post_time_and_author_name
import kotlinx.android.synthetic.main.item_post.view.user_picture

class PostListAdapter(
    val context: Context,
    val currentUserId: Int?
) : RecyclerView.Adapter<PostListAdapter.PostHolder>() {
    private var posts = mutableListOf<Post>()
    fun setPosts(posts: Collection<Post>) {
        this.posts = posts.toMutableList()
        notifyDataSetChanged()
    }

    var mayDelete = false
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var deletePost: ((postId: Int) -> Unit)? = null
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: PostHolder, position: Int) {
        if (position in 0 until itemCount) {
            posts[position].let { post ->
                holder.bind(post, mayDelete) {
                    deletePost?.invoke(post.id)
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int) =
        PostHolder(parent.inflate(R.layout.item_post, false), context, currentUserId)

    class PostHolder(
        val view: View,
        val context: Context,
        val currentUserId: Int?
    ) : RecyclerView.ViewHolder(view) {

        private var deleteBoundPost: (() -> Unit)? = null
        private val pictureAdapter = PostPictureListAdapter(context)

        init {
            view.user_picture.setCorners(15f, 15f, 15f, 15f)
            view.post_pictures.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = pictureAdapter
            }
            view.post_delete_button.setOnClickListener {
                AlertDialog.Builder(context)
                    .setMessage(R.string.delete_post_confirmation)
                    .setPositiveButton(R.string.delete_post) { _, _ ->
                        deleteBoundPost?.invoke()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .show()
            }
        }

        fun bind(post: Post, mayDelete: Boolean, deleteBoundPost: () -> Unit) {
            this.deleteBoundPost = deleteBoundPost
            view.apply {
                user_picture.let {
                    Glide.with(this)
                        .load(Utils.getUserPhotoURL(post.author, Utils.PhotoType.MINI, 35))
                        .error(R.drawable.default_user_picture)
                        .fitCenter()
                        .centerCrop()
                        .into(it)
                    it.setOnClickListener {
                        ProfileActivity.start(context, post.author)
                    }
                }
                post_body.text = post.body
                post_time_and_author_name.text = context.getString(R.string.created_at_by_author,
                    formatDateTime(context, post.createdAt.time,
                        FORMAT_SHOW_WEEKDAY or FORMAT_SHOW_DATE or FORMAT_SHOW_TIME),
                    post.author.name)
                post_delete_button.visibility = if (mayDelete || post.author.id == currentUserId) VISIBLE else GONE
            }

            pictureAdapter.setPictures(post.pictures.orEmpty())
        }
    }
}
