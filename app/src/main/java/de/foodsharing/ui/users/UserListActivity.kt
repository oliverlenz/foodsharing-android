package de.foodsharing.ui.users

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.R
import de.foodsharing.di.Injectable
import de.foodsharing.model.User
import de.foodsharing.ui.base.BaseActivity
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.profile.ProfileActivity
import de.foodsharing.utils.getDisplayName
import kotlinx.android.synthetic.main.activity_user_list.progress_bar
import kotlinx.android.synthetic.main.activity_user_list.recycler_view
import kotlinx.android.synthetic.main.activity_user_list.toolbar
import javax.inject.Inject

class UserListActivity : BaseActivity(), Injectable {

    companion object {
        private const val EXTRA_CONVERSATION_ID = "conversation_id"
        private const val EXTRA_TITLE = "title"

        fun start(context: Context, conversationId: Int, title: String) {
            val extras = bundleOf(
                    EXTRA_CONVERSATION_ID to conversationId,
                    EXTRA_TITLE to title
            )
            val intent = Intent(context, UserListActivity::class.java).putExtras(extras)
            context.startActivity(intent)
        }
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: UserListViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: UserListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        rootLayoutID = R.id.user_list_content
        setContentView(R.layout.activity_user_list)
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title =
            if (intent.hasExtra(EXTRA_TITLE)) intent.getStringExtra(EXTRA_TITLE)
            else ""

        if (intent.hasExtra(EXTRA_CONVERSATION_ID)) {
            viewModel.conversationId = intent.getIntExtra(EXTRA_CONVERSATION_ID, -1)
        }

        adapter = UserListAdapter(this::onViewUser, this)

        val layoutManager = LinearLayoutManager(this)
        recycler_view.layoutManager = layoutManager
        recycler_view.adapter = adapter

        bindViewModel()
    }

    private fun bindViewModel() {
        viewModel.users.observe(this) {
            adapter.setUsers(it.sortedBy { user -> user.getDisplayName(this) })
        }

        viewModel.isLoading.observe(this) {
            progress_bar.visibility = if (it) {
                View.VISIBLE
            } else {
                View.GONE
            }
        }

        viewModel.showError.observe(this, EventObserver {
            showErrorMessage(getString(it))
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        android.R.id.home -> {
            onBackPressed()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    override fun finish() {
        super.finish()
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right)
    }

    private fun showErrorMessage(error: String) {
        showMessage(error, duration = Snackbar.LENGTH_LONG)
    }

    private fun onViewUser(user: User) {
        ProfileActivity.start(this, user)
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
    }
}
