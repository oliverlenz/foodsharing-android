package de.foodsharing.ui.base

import android.app.Activity
import android.content.Context
import android.widget.RadioGroup
import android.widget.SeekBar
import androidx.appcompat.app.AlertDialog
import de.foodsharing.R
import kotlinx.android.synthetic.main.location_filter_component.view.distance_label
import kotlinx.android.synthetic.main.location_filter_component.view.distance_seek_bar
import kotlinx.android.synthetic.main.location_filter_component.view.distance_value_label
import kotlinx.android.synthetic.main.location_filter_component.view.location_type_radio_group
import kotlinx.android.synthetic.main.location_filter_component.view.location_type_text

class LocationFilterComponent {

    enum class LocationType {
        HOME, CURRENT_LOCATION
    }

    companion object {
        fun showDialog(
            activity: Activity,
            locationType: LocationType,
            distance: Int,
            result: (LocationType, Int) -> Unit
        ) {
            val view = activity.layoutInflater.inflate(R.layout.location_filter_component, null)

            // add listeners
            view.distance_seek_bar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                    view.distance_value_label.text = "%d km".format(progress)
                }

                override fun onStartTrackingTouch(seekBar: SeekBar?) {
                }

                override fun onStopTrackingTouch(seekBar: SeekBar?) {
                }
            })
            view.location_type_radio_group.setOnCheckedChangeListener { _, checked ->
                updateLayout(activity, checked, view)
            }

            // set current values
            view.location_type_radio_group.check(typeToRadioButton(locationType))
            view.distance_seek_bar.progress = distance
            view.distance_value_label.text = "%d km".format(distance)
            updateLayout(activity, typeToRadioButton(locationType), view)

            // create and show the dialog
            val builder = AlertDialog.Builder(activity)
            builder.setTitle(activity.getString(R.string.location_dialog_title))
                .setView(view)
                .setPositiveButton(android.R.string.ok) { dialog, id ->
                    val newDistance = (view.distance_seek_bar as SeekBar).progress
                    val newType = radioButtonToType((view.location_type_radio_group as RadioGroup).checkedRadioButtonId)
                    result(newType, newDistance)
                    dialog.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { dialog, _ ->
                    dialog.dismiss()
                }
            builder.create().show()
        }

        private fun typeToRadioButton(type: LocationType) = when (type) {
            LocationType.CURRENT_LOCATION -> R.id.location_type_radio_current
            else -> R.id.location_type_radio_home
        }

        private fun radioButtonToType(id: Int) = when (id) {
            R.id.location_type_radio_current -> LocationType.CURRENT_LOCATION
            else -> LocationType.HOME
        }

        private fun updateLayout(context: Context, checkedType: Int, view: android.view.View) {
            val text: String
            val progressEnabled: Boolean
            when (checkedType) {
                R.id.location_type_radio_current -> {
                    text = context.getString(R.string.location_dialog_location_type_current)
                    progressEnabled = true
                }
                else -> {
                    text = context.getString(R.string.location_dialog_location_type_home)
                    progressEnabled = false
                }
            }
            view.location_type_text.text = text
            view.distance_seek_bar.isEnabled = progressEnabled
            view.distance_label.isEnabled = progressEnabled
            view.distance_value_label.isEnabled = progressEnabled
        }
    }
}
