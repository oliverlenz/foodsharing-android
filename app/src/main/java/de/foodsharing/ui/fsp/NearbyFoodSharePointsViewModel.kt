package de.foodsharing.ui.fsp

import androidx.lifecycle.MutableLiveData
import de.foodsharing.R
import de.foodsharing.api.FoodSharePointAPI
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.ui.base.BaseViewModel
import de.foodsharing.ui.base.Event
import de.foodsharing.utils.DEFAULT_FSP_DISTANCE_KM
import de.foodsharing.utils.UserLocation
import io.reactivex.schedulers.Schedulers
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject

class NearbyFoodSharePointsViewModel @Inject constructor(
    private val foodSharePointAPI: FoodSharePointAPI,
    private val userLocation: UserLocation
) : BaseViewModel() {

    // status and output data
    val foodSharePoints = MutableLiveData<List<Pair<FoodSharePoint, Double>>>()
    val isLoading = MutableLiveData<Boolean>().apply {
        value = true
    }
    val showError = MutableLiveData<Event<Int>>()

    // parameters
    val distance = MutableLiveData<Int>().apply {
        value = DEFAULT_FSP_DISTANCE_KM
    }

    private val refreshEvents = BehaviorSubject.createDefault<Any>(true)

    init {
        request(refreshEvents.doOnNext {
            isLoading.postValue(true)
        }.switchMap {
            getNearbyFSPs().subscribeOn(Schedulers.io())
        }, {
            isLoading.value = false
            foodSharePoints.value = it
        }, {
            isLoading.value = false
            showError.value = Event(R.string.error_unknown)
        })
    }

    /**
     * Creates an observable that lists all nearby FoodSharePoints based on [userLocation] and [distance].
     */
    private fun getNearbyFSPs() = userLocation.getUsersLocation().switchMap { refCoordinate ->
        val fspsDistance = distance.value ?: DEFAULT_FSP_DISTANCE_KM
        foodSharePointAPI.getNearby(refCoordinate.lat, refCoordinate.lon, fspsDistance)
            .map { response ->
                response.map { fsp ->
                    Pair(fsp, fsp.toCoordinate().distanceTo(refCoordinate))
                }.sortedBy {
                    it.second
                }
            }
    }.defaultIfEmpty(emptyList())

    fun reload() {
        refreshEvents.onNext(true)
    }
}
