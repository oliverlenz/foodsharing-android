package de.foodsharing.ui.fsp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import de.foodsharing.R
import de.foodsharing.services.PreferenceManager
import de.foodsharing.ui.base.BaseFragment
import de.foodsharing.ui.base.EventObserver
import de.foodsharing.ui.base.LocationFilterComponent
import de.foodsharing.utils.NonScrollingLinearLayoutManager
import de.foodsharing.utils.UserLocation
import kotlinx.android.synthetic.main.fragment_nearby_food_share_points.no_fsps_label
import kotlinx.android.synthetic.main.fragment_nearby_food_share_points.progress_bar
import kotlinx.android.synthetic.main.fragment_nearby_food_share_points.recycler_view
import kotlinx.android.synthetic.main.fragment_nearby_food_share_points.view.nearby_fsps_settings_button
import kotlinx.android.synthetic.main.fragment_nearby_food_share_points.view.recycler_view
import javax.inject.Inject

class NearbyFoodSharePointsFragment : BaseFragment() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    @Inject
    lateinit var userLocation: UserLocation

    @Inject
    lateinit var preferenceManager: PreferenceManager

    private val viewModel: NearbyFoodSharePointsViewModel by viewModels { viewModelFactory }

    private lateinit var adapter: NearbyFoodSharePointsListAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_nearby_food_share_points, container, false)
        view.recycler_view.layoutManager = NonScrollingLinearLayoutManager(activity)

        adapter = NearbyFoodSharePointsListAdapter({ foodSharePoint ->
            FoodSharePointActivity.start(requireContext(), foodSharePoint.id)
            activity?.overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left)
        }, requireContext())
        view.recycler_view.adapter = adapter

        bindViewModel()

        userLocation.currentCoordinates.observe(viewLifecycleOwner) {
            it.let {
                val prevCoordinate = userLocation.previousCoordinate
                if (it.lat != 0.0 && it.lon != 0.0 && prevCoordinate.lat != it.lat && prevCoordinate.lon != it.lon) {
                    viewModel.reload()
                }
            }
        }

        view.nearby_fsps_settings_button.setOnClickListener {
            showSettingsDialog()
        }

        return view
    }

    private fun bindViewModel() {
        viewModel.isLoading.observe(viewLifecycleOwner) {
            if (it) {
                no_fsps_label.visibility = View.GONE
                progress_bar.visibility = View.VISIBLE
            } else {
                progress_bar.visibility = View.GONE
            }
        }

        viewModel.showError.observe(viewLifecycleOwner, EventObserver {
            showMessage(getString(it))
        })

        viewModel.foodSharePoints.observe(viewLifecycleOwner) {
            if (it.isEmpty()) {
                recycler_view.visibility = View.GONE
                no_fsps_label.visibility = View.VISIBLE
            } else {
                recycler_view.visibility = View.VISIBLE
                no_fsps_label.visibility = View.GONE

                adapter.setFoodSharePoints(it)
            }
        }
    }

    private fun showSettingsDialog() {
        LocationFilterComponent.showDialog(
            requireActivity(), preferenceManager.nearbyFSPsLocationType,
            preferenceManager.nearbyFSPsDistance
        ) { locationType, distance ->
            preferenceManager.nearbyFSPsLocationType = locationType
            preferenceManager.nearbyFSPsDistance = distance
        }
    }

    fun reload() {
        viewModel.reload()
    }
}
