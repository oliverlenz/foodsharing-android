package de.foodsharing.ui.fsp

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import de.foodsharing.R
import de.foodsharing.model.FoodSharePoint
import de.foodsharing.utils.Utils
import de.foodsharing.utils.inflate
import kotlinx.android.synthetic.main.item_foodsharepoint.view.item_distance
import kotlinx.android.synthetic.main.item_foodsharepoint.view.item_icon
import kotlinx.android.synthetic.main.item_foodsharepoint.view.item_name
import java.util.Locale

class NearbyFoodSharePointsListAdapter(
    private val onClickListener: (FoodSharePoint) -> Unit,
    val context: Context
) : RecyclerView.Adapter<NearbyFoodSharePointsListAdapter.FoodSharePointHolder>() {
    private var foodSharePoints = emptyList<Pair<FoodSharePoint, Double>>()

    override fun getItemCount() = foodSharePoints.size

    override fun onBindViewHolder(holder: FoodSharePointHolder, position: Int) {
        if (position in 0 until itemCount) {
            holder.bind(foodSharePoints[position].first, foodSharePoints[position].second)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewInt: Int): FoodSharePointHolder {
        return FoodSharePointHolder(parent.inflate(R.layout.item_foodsharepoint, false), onClickListener, context)
    }

    fun setFoodSharePoints(foodSharePoints: List<Pair<FoodSharePoint, Double>>) {
        this.foodSharePoints = foodSharePoints
        notifyDataSetChanged()
    }

    class FoodSharePointHolder(
        val view: View,
        private val onClickListener: (FoodSharePoint) -> Unit,
        val context: Context
    ) : RecyclerView.ViewHolder(view), View.OnClickListener {

        private var foodSharePoint: FoodSharePoint? = null

        init {
            view.setOnClickListener(this)
        }

        override fun onClick(v: View) {
            foodSharePoint?.let {
                onClickListener(it)
            }
        }

        fun bind(foodSharePoint: FoodSharePoint, distance: Double) {
            this.foodSharePoint = foodSharePoint

            view.item_icon.setImageDrawable(Utils.getFspMarkerIcon(context))
            view.item_name.text = foodSharePoint.name
            view.item_distance.text = Utils.formatDistance(distance).toUpperCase(Locale.getDefault())
        }
    }
}
