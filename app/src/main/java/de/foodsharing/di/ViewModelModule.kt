package de.foodsharing.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap
import de.foodsharing.ui.basket.BasketViewModel
import de.foodsharing.ui.baskets.MyBasketsViewModel
import de.foodsharing.ui.baskets.NearbyBasketsViewModel
import de.foodsharing.ui.community.CommunityViewModel
import de.foodsharing.ui.conversation.ConversationViewModel
import de.foodsharing.ui.conversations.ConversationsViewModel
import de.foodsharing.ui.editbasket.EditBasketViewModel
import de.foodsharing.ui.fsp.FoodSharePointViewModel
import de.foodsharing.ui.fsp.NearbyFoodSharePointsViewModel
import de.foodsharing.ui.login.LoginViewModel
import de.foodsharing.ui.main.MainViewModel
import de.foodsharing.ui.map.MapViewModel
import de.foodsharing.ui.newbasket.NewBasketViewModel
import de.foodsharing.ui.pickup.PickupViewModel
import de.foodsharing.ui.pickups.PickupsViewModel
import de.foodsharing.ui.posts.PostsViewModel
import de.foodsharing.ui.profile.ProfileViewModel
import de.foodsharing.ui.users.UserListViewModel

@Module
abstract class ViewModelModule {

    @Binds
    abstract fun bindViewModelFactory(factory: DaggerViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(LoginViewModel::class)
    abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MainViewModel::class)
    abstract fun bindMainViewModel(viewModel: MainViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MyBasketsViewModel::class)
    abstract fun bindMyBasketsViewModel(viewModel: MyBasketsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NearbyBasketsViewModel::class)
    abstract fun bindNearbyBasketsViewModel(viewModel: NearbyBasketsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(FoodSharePointViewModel::class)
    abstract fun bindFSPViewModel(fspViewModel: FoodSharePointViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ProfileViewModel::class)
    abstract fun bindProfileViewModel(profileViewModel: ProfileViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindMapViewModel(mapViewModel: MapViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConversationViewModel::class)
    abstract fun bindConversationViewModel(viewModel: ConversationViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ConversationsViewModel::class)
    abstract fun bindConversationsViewModel(viewModel: ConversationsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(BasketViewModel::class)
    abstract fun bindBasketViewModel(viewModel: BasketViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(UserListViewModel::class)
    abstract fun bindUserListViewModel(viewModel: UserListViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PostsViewModel::class)
    abstract fun bindPostsViewModel(viewModel: PostsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NewBasketViewModel::class)
    abstract fun bindNewBasketViewModel(viewModel: NewBasketViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(EditBasketViewModel::class)
    abstract fun bindEditBasketViewModel(viewModel: EditBasketViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(NearbyFoodSharePointsViewModel::class)
    abstract fun bindNearbyFSPsViewModel(viewModel: NearbyFoodSharePointsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PickupsViewModel::class)
    abstract fun bindPickupsViewModel(viewModel: PickupsViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(CommunityViewModel::class)
    abstract fun bindCommunityViewModel(viewModel: CommunityViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(PickupViewModel::class)
    abstract fun bindPickupViewModel(viewModel: PickupViewModel): ViewModel
}
