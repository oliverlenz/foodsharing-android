package de.foodsharing.api

import de.foodsharing.model.PostsResponse
import de.foodsharing.model.SendPostResponse
import io.reactivex.Observable
import retrofit2.http.DELETE
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

interface PostsAPI {
    enum class Target(val value: String) {
        PROFILE("foodsaver"),
        FOOD_SHARE_POINT("fairteiler")
    }

    @GET("/api/wall/{target}/{id}")
    fun getPosts(
        @Path("target") target: String,
        @Path("id") id: Int
    ): Observable<PostsResponse>

    @FormUrlEncoded
    @POST("/api/wall/{target}/{id}")
    fun sendPost(
        @Path("target") target: String,
        @Path("id") id: Int,
        @Field("body") body: String
    ): Observable<SendPostResponse>

    @DELETE("/api/wall/{target}/{targetId}/{id}")
    fun deletePost(
        @Path("target") target: String,
        @Path("targetId") targetId: Int,
        @Path("id") id: Int
    ): Observable<Unit>
}
