package de.foodsharing.api

import android.util.Log
import com.google.gson.Gson
import com.google.gson.JsonParser
import de.foodsharing.utils.BASE_URL
import de.foodsharing.utils.ConnectivityReceiver
import de.foodsharing.utils.LOG_TAG
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.socket.client.IO
import io.socket.client.Socket
import io.socket.client.Socket.EVENT_CONNECT
import io.socket.client.Socket.EVENT_CONNECT_ERROR
import io.socket.client.Socket.EVENT_ERROR
import okhttp3.OkHttpClient
import org.json.JSONObject
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DefaultWebsocketAPI @Inject constructor(
    private val httpClient: OkHttpClient,
    private val gson: Gson
) : WebsocketAPI {

    private val io: Socket

    init {
        val opts = IO.Options().apply {
            path = "/websocket/socket.io/"
            callFactory = httpClient
            webSocketFactory = httpClient
            transports = arrayOf("websocket")
        }

        fun onError(vararg args: Any?) {
            val arg = args[0]
            when (arg) {
                is Throwable -> Log.w(LOG_TAG, arg.localizedMessage, arg)
                else -> Log.w(LOG_TAG, "$arg")
            }
            close()
        }

        io = IO.socket(BASE_URL, opts).apply {
            on(EVENT_CONNECT) {
                emit("register")
                emit("visibilitychange", false)
            }
            on(EVENT_ERROR, ::onError)
            on(EVENT_CONNECT_ERROR, ::onError)
        }
    }

    private val websocketObservable: Observable<WebsocketAPI.Message> by lazy {
        Observable.create<WebsocketAPI.Message> { emitter ->
            if (!this.io.connected()) this.io.connect()

            fun onConv(vararg args: Any?) {
                val data = args[0] as JSONObject
                if ("push" == data.optString("m")) {
                    val jsonString = data.getString("o")
                    val jsonElement = JsonParser.parseString(jsonString)
                    if (!jsonElement.isJsonObject) return
                    val jsonObject = jsonElement.asJsonObject
                    // Try to find the correct message type
                    val msg = if (jsonObject.has("cid") && jsonObject.has("message")) {
                        gson.fromJson(jsonObject, WebsocketAPI.ConversationMessage::class.java)
                    } else if (jsonObject.has("cid") &&
                        jsonObject.has("fsId") && jsonObject.has("body")) {
                        gson.fromJson(jsonObject, WebsocketAPI.OldConversationMessage::class.java)
                            .toConversationMessage()
                    } else {
                        // Unidentified message -> discard it
                        return
                    }
                    emitter.onNext(msg)
                }
            }

            fun onError(vararg args: Any?) {
                val arg = args[0]
                when (arg) {
                    is Throwable -> if (!emitter.isDisposed) emitter.onError(arg)
                }
            }

            io.apply {
                on("conv", ::onConv)
                on(EVENT_ERROR, ::onError)
                on(EVENT_CONNECT_ERROR, ::onError)
            }

            emitter.setDisposable(object : Disposable {

                var disposed = false

                override fun isDisposed(): Boolean {
                    return disposed
                }

                override fun dispose() {
                    disposed = true
                    io.apply {
                        off("conv", ::onConv)
                        off(EVENT_ERROR, ::onError)
                        off(EVENT_CONNECT_ERROR, ::onError)
                    }
                }
            })
        }.retryWhen {
            ConnectivityReceiver.observe().filter { isConnected -> isConnected }
        }.share()
    }

    override fun subscribe(): Observable<WebsocketAPI.Message> {
        return websocketObservable
    }

    override fun close() {
        if (io.connected())
            io.disconnect()
    }
}
