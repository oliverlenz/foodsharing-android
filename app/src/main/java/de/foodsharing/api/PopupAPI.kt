package de.foodsharing.api

import de.foodsharing.model.Popup
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Url

interface PopupAPI {
    @GET
    fun current(@Url url: String): Observable<List<Popup>>
}
