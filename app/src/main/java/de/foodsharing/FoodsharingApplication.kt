package de.foodsharing

import android.app.Activity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.multidex.MultiDexApplication
import com.jakewharton.threetenabp.AndroidThreeTen
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import dagger.android.support.AndroidSupportInjection
import de.foodsharing.di.DaggerApplicationComponent
import de.foodsharing.di.Injectable
import de.foodsharing.notifications.PushService
import de.foodsharing.services.PreferenceManager
import de.foodsharing.utils.LocationFinder
import de.foodsharing.utils.OsmdroidUtils
import de.foodsharing.utils.captureException
import io.reactivex.plugins.RxJavaPlugins
import io.sentry.Sentry
import io.sentry.android.AndroidSentryClientFactory
import javax.inject.Inject

class FoodsharingApplication : MultiDexApplication(), HasAndroidInjector {

    @Inject
    lateinit var activityInjector: DispatchingAndroidInjector<Any>

    @Inject
    lateinit var pushService: PushService

    @Inject
    lateinit var preferences: PreferenceManager

    override fun androidInjector(): AndroidInjector<Any> = activityInjector

    override fun onCreate() {
        super.onCreate()

        // Necessary to initialize time zone information
        AndroidThreeTen.init(this)

        DaggerApplicationComponent.builder().application(this).build().inject(this)

        Sentry.init(AndroidSentryClientFactory(applicationContext))
            .addShouldSendEventCallback { (preferences.isSentryEnabled == true) }

        // Do not crash the app, pass them to our general exception handler
        RxJavaPlugins.setErrorHandler(::captureException)

        this.registerActivityLifecycleCallbacks(object : ActivityLifecycleCallbacks {
            override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
                handleActivity(activity)
            }

            override fun onActivityPaused(activity: Activity) {
            }

            override fun onActivityResumed(activity: Activity) {
            }

            override fun onActivityStarted(activity: Activity) {
            }

            override fun onActivityDestroyed(activity: Activity) {
            }

            override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {
            }

            override fun onActivityStopped(activity: Activity) {
            }
        })

        LocationFinder.instance.initialise(this)
        OsmdroidUtils.initialise(applicationContext)
    }

    private fun handleActivity(activity: Activity) {
        if (activity is HasAndroidInjector || activity is Injectable) {
            AndroidInjection.inject(activity)
        }
        if (activity is FragmentActivity) {
            activity.supportFragmentManager.registerFragmentLifecycleCallbacks(
                    object : FragmentManager.FragmentLifecycleCallbacks() {
                        override fun onFragmentCreated(
                            fm: FragmentManager,
                            f: Fragment,
                            savedInstanceState: Bundle?
                        ) {
                            if (f is Injectable) {
                                AndroidSupportInjection.inject(f)
                            }
                        }
                    }, true
            )
        }
    }
}
