package de.foodsharing.model

data class Community(
    val name: String,
    val description: String?
)
