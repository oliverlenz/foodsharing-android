package de.foodsharing.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable
import java.util.Date

// TODO: unify with UserPickup in backend
data class StorePickup(
    val date: Date,
    @SerializedName("isAvailable")
    val isAvailable: Boolean,
    @SerializedName("totalSlots")
    val totalSlots: Int,
    @SerializedName("occupiedSlots")
    val occupiedSlots: List<StorePickupSlot>
)

data class StorePickupSlot(
    @SerializedName("isConfirmed")
    val isConfirmed: Boolean,
    val profile: StorePickupSlotProfile
) : Serializable

/**
 * Represents a person that is signed in to a pickup slot.
 */
data class StorePickupSlotProfile(
    val id: Int,
    val name: String?,
    val avatar: String?,
    @SerializedName("sleepStatus")
    val sleepStatus: Int?,
    val mobile: String?,
    val landline: String?,
    @SerializedName("isManager")
    val isManager: Boolean
) : Serializable
