package de.foodsharing.utils

import android.content.res.Resources
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.ColorInt
import androidx.annotation.LayoutRes
import androidx.core.os.ConfigurationCompat
import com.google.android.material.snackbar.Snackbar
import de.foodsharing.BuildConfig
import de.foodsharing.model.Popup
import de.foodsharing.model.PopupCondition
import de.foodsharing.model.PopupConditionType

/**
 * Sets the background color of a snackbar.
 * @param colorInt the background color
 *
 * @return this Snackbar
 */
fun Snackbar.withColor(@ColorInt colorInt: Int): Snackbar {
    view.setBackgroundColor(colorInt)
    return this
}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}

/**
 * Checks whether all conditions of the [Popup] are satisfied.
 */
fun Popup.isSatisfied(resources: Resources): Boolean {
    return conditions.fold(true) { acc, condition ->
        acc && condition.isSatisfied(resources)
    }
}

/**
 * Checks whether the [PopupCondition] is satisfied.
 */
fun PopupCondition.isSatisfied(resources: Resources): Boolean {
    return when (type ?: return false) {
        PopupConditionType.VERSION_CODE ->
            Utils.compareIntToPattern(BuildConfig.VERSION_CODE, value)
        PopupConditionType.LOCALE -> {
            val locale = ConfigurationCompat.getLocales(resources.configuration).get(0)
            locale.language == value
        }
        PopupConditionType.BUILD_TYPE -> value == BuildConfig.BUILD_TYPE
        PopupConditionType.FLAVOR -> value == BuildConfig.FLAVOR
    }
}

fun View.setVisible() {
    visibility = View.VISIBLE
}

fun View.setGone() {
    visibility = View.GONE
}

fun Any.setVisible(vararg views: View) =
    views.forEach { it.setVisible() }

fun Any.setGone(vararg views: View) =
    views.forEach { it.setGone() }

inline fun <T1 : Any, T2 : Any, R : Any> safeLet(p1: T1?, p2: T2?, block: (T1, T2) -> R?): R? {
    return if (p1 != null && p2 != null) block(p1, p2) else null
}
