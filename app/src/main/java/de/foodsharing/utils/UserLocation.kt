package de.foodsharing.utils

import de.foodsharing.model.Coordinate
import de.foodsharing.services.ProfileService
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Utility class class that provides the user's location from different sources. It tries to request the
 * current location from the device and uses the user profile as fallback.
 */
class UserLocation @Inject constructor(
    private val profileService: ProfileService,
    private val currentUserLocation: CurrentUserLocation
) {

    var currentCoordinates = (currentUserLocation.currentUserCoordinates)
    var previousCoordinate = Coordinate(0.0, 0.0)

    /**
     * Returns the coordinates set in the user's profile.
     */
    private fun getProfileCoordinates() = profileService.current().switchMap { profile ->
        profile.coordinates?.let {
            Observable.just(it)
        } ?: Observable.empty()
    }

    /**
     * Returns the phone's current location.
     */
    private fun getPhoneCoordinates() = currentCoordinates.value?.let {
        Observable.just(it)
    } ?: Observable.empty()

    /**
     * Returns the current location based on the phone's location or the address set in the profile.
     */
    fun getUsersLocation(): Observable<Coordinate> {
        return if ((currentCoordinates.value == null) ||
            (currentCoordinates.value?.lat == 0.0 && currentCoordinates.value?.lon == 0.0)) {
            getProfileCoordinates()
        } else {
            getPhoneCoordinates()
        }
    }

    /**
     * Updates and returns the user's current location.
     */
    fun updateUsersLocation() {
        currentUserLocation.findLocation()
    }
}
