package de.foodsharing.services

import com.franmontiel.persistentcookiejar.ClearableCookieJar
import de.foodsharing.api.LoginRequest
import de.foodsharing.api.UserAPI
import de.foodsharing.api.WebsocketAPI
import de.foodsharing.model.User
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import retrofit2.HttpException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class AuthService @Inject constructor(
    private val api: UserAPI,
    private val ws: WebsocketAPI,
    private val cookieJar: ClearableCookieJar,
    private val preferences: PreferenceManager
) {

    @Volatile
    var currentUser: User? = null

    fun currentUser(): Observable<User> {
        return currentUser?.let {
            Observable.just(it)
        } ?: api.currentUser().doOnNext {
            currentUser = it
        }
    }

    fun check(): Observable<Boolean> {
        return api.currentUser()
                .doOnNext { user -> currentUser = user }
                .map { true }
                .onErrorReturn { error ->
                    if (error is HttpException && error.code() == 401) {
                        clear()
                        false
                    } else {
                        throw(error)
                    }
                }
    }

    fun login(data: LoginRequest): Observable<User> {
        return api.login(data)
            .doOnNext { user -> currentUser = user }
            .doOnNext { preferences.isLoggedIn = true }
    }

    fun logout(): Observable<Unit> {
        return api.logout().doFinally { clear() }
    }

    fun clear() {
        currentUser = null
        ws.close()
        cookieJar.clear()
        preferences.isLoggedIn = false
    }
}
