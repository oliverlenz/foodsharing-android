package de.foodsharing.services

import de.foodsharing.api.UserAPI
import de.foodsharing.model.Profile
import de.foodsharing.utils.testing.OpenForTesting
import io.reactivex.Observable
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
@OpenForTesting
class ProfileService @Inject constructor(
    private val userAPI: UserAPI
) {

    fun current(): Observable<Profile> {
        return Observable.concat(
                userAPI.currentUserDetails("only-if-cached").onErrorResumeNext(Observable.empty()),
                userAPI.currentUserDetails("no-cache"))
                .distinctUntilChanged()
                .map {
                    if (it.coordinates?.lat == 0.0 && it.coordinates.lon == 0.0) {
                        // Coordinate is not set in profile, make this explicit
                        it.copy(coordinates = null)
                    } else {
                        it
                    }
                }
    }
}
