package de.foodsharing.notifications

import android.util.Base64
import com.google.capillary.internal.CapillaryCiphertext
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.google.protobuf.ByteString
import dagger.android.AndroidInjection
import de.foodsharing.services.PreferenceManager
import java.io.IOException
import java.nio.ByteBuffer
import java.security.GeneralSecurityException
import javax.inject.Inject

/**
 * Service handling incoming notification from the OS.
 *
 */
class FoodsharingMessagingService : FirebaseMessagingService() {
    @Inject
    lateinit var capillaryHandler: FoodsharingCapillaryHandler

    @Inject
    lateinit var preferences: PreferenceManager

    override fun onCreate() {
        AndroidInjection.inject(this)
        super.onCreate()
    }

    override fun onNewToken(token: String) {
        preferences.pushToken = token
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        // Only handle messages containing a a payload
        if (remoteMessage.data.isNotEmpty()) {
            handleDataMessage(remoteMessage.data)
        }
    }

    /**
     * Handle incoming messages
     */
    private fun handleDataMessage(dataMap: Map<String, String>) {
        if (preferences.pushNotificationsEnabled != true) {
            // Ignore data messages if push notification are disabled
            return
        }
        try {
            val keychainUniqueId = dataMap["k"] ?: return
            val serialNumber = dataMap["s"] ?: return
            val body = dataMap["b"] ?: return

            // Initializes capillary (installs relevant crypto if necessary and )
            CapillaryUtils.initialize(this)

            // Get the encryption algorithm and the ciphertext bytes.
            val rawCiphertext = Base64.decode(body, Base64.URL_SAFE)
            val buffer = ByteBuffer.wrap(rawCiphertext)
            buffer.position(16)

            val cipherObject = CapillaryCiphertext.newBuilder()
                    .setIsAuthKey(false)
                    .setCiphertext(ByteString.copyFrom(rawCiphertext))
                    .setKeySerialNumber(serialNumber.toInt())
                    .setKeychainUniqueId(keychainUniqueId)
                    .build()

            // Handle ciphertext.
            CapillaryUtils.getKeyManager(this)
                    .decrypterManager.decrypt(cipherObject.toByteArray(), capillaryHandler, null)
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }
}
